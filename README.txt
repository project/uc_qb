********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Quickbooks Ubercart Integration
Author: Sarva Bryant <sarva at aliandesign dot com>
Drupal: 6.x
********************************************************************
DESCRIPTION:

Integrate Ubercart with QuickBooks using the QuickBooks API and
Web Connector. Simply install the required modules, install the
web connector and quickbooks will automatically import the latest
order information from an Ubercart periodically.

********************************************************************
REQUIREMENTS:

* SOAP
* PHP5
* HTTPS setup for https://yoursite.com/qbwc with a valid certificate
* Quickbooks installed
* Quickbooks SDK installed (requires login)
  http://developer.intuit.com/technical_resources/default.aspx?id=1492
* Quickbooks Web Connector installed
  http://marketplace.intuit.com/webconnector
* Quickbooks API and Web Connector modules patched
  http://drupal.org/project/qb
  http://cvs.drupal.org/viewvc.py/drupal/contributions/modules/qb/
  http://drupal.org/node/298079
  http://drupal.org/files/issues/qb.patch


********************************************************************
INSTALLATION:

1. Download the Quickbooks API module

   Project Page: http://drupal.org/project/qb
   CVS: http://cvs.drupal.org/viewvc.py/drupal/contributions/modules/qb

2. Install the Quickbooks API, Web Connector, and this module.

3. Setup Web Connector settings
   http://yoursite.com/admin/settings/qb

4. Download the Web Connector Configuration file
   http://yoursite.com/admin/settings/qb/qwc

5. Setup Ubercart Quickbook settings if needed
   http://yoursite.com/admin/store/settings/qb

6. Make sure Quickbooks and Web Connector are running and load the
   QBW configuration file you downloaded into the Web Connector.

7. Enter the password of the user you set for the Web Connector settings.

8. Check the application and click "Update" or let it run if set to
   auto-run.

9. Watch QuickBooks populate with your Ubercart data!


********************************************************************

TODO:

