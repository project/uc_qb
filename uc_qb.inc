<?php

// This array defines the values for which the user can set a global default,
// and then override it at the class and product level.
// For each value, we have the following settings:
// Display: The nice name for the field for display in the UI.
// Field: The field in a SalesReceiptAdd Query
// Type: The source of our values.
// Account Type: The type of account (to limit values in autocomplete / dropdowns)
// Default: A default value used if nothing else is set (ie settings pages not yet visited)
// Contexts: Where used (Pushing a product to Quickbooks and/or adding a sale or sale line)
//   and whether required (value).
// Product Types: If unset, applies to all product types. Otherwise, an array saying which
//   product types use this field.
$GLOBALS["FieldList"] = array(
  'product_type' => array(
    'Display' => 'Product Type',
    'Type' => 'Product Type',
    'Contexts' => array('Create Product' => 1),
    'Import Only' => TRUE,
    'Group' => 'product',
    'Weight' => 0,
  ),
  'product' => array(
    'Display' => 'Product',
    'Field' => 'ItemRef',
    'Type' => 'Item',
    'Contexts' => array('Create Product' => 1, 'Sale' => 1),
    'Group' => 'product',
    'Weight' => 10,
    'Instance Only' => TRUE,
  ),
  'sales_tax' => array(
    'Display' => 'Tax Code',
    'Field' => 'SalesTaxCodeRef',
    'ItemAddField' => 'SalesTaxCodeRef',
    'Type' => 'TaxCode',
    'Variable' => 'uc_qb_tax_types',
    'Contexts' => array('Create Product' => 0),
    'Group' => 'tax_code',
    'Import Only' => TRUE,
    'Weight' => 20,
  ),
  'income' => array(
    'Display' => 'Income Account',
    'Field' => 'ItemRef',
    'ItemAddField' => 'IncomeAccountRef',
    'Type' => 'Account',
    'Account Type' => 'Income',
    'Contexts' => array('Create Product' => 0),
    'Export Config' => 'uc_qb_add_account_method',
    'Group' => 'account',
    'Weight' => 30,
  ),
  'cogs' => array(
    'Display' => 'Cost of Goods Sold Account',
    'Field' => 'COGSAccountRef',
    'ItemAddField' => 'COGSAccountRef',
    'Type' => 'Account',
    'Account Type' => 'CostOfGoodsSold',
    'Contexts' => array('Create Product' => 0),
    'Group' => 'account',
    'Product Types' => array('Inventory item'),
    'Weight' => 40,
  ),
  'asset_account' => array(
    'Display' => 'Asset Account',
    'Field' => 'AssetAccountRef',
    'ItemAddField' => 'AssetAccountRef',
    'Type' => 'Account',
    'Account Type' => 'OtherAsset',
    'Contexts' => array('Create Product' => 0),
    'Group' => 'account',
    'Product Types' => array('Inventory item'),
    'Weight' => 50,
  ),
  'expense_account' => array(
    'Display' => 'Expense Account',
    'Field' => 'ExpenseAccountRef',
    'ItemAddField' => 'ExpenseAccountRef',
    'Type' => 'Account',
    'Account Type' => 'Expense',
    'Contexts' => array('Create Product' => 0),
    'Group' => 'account',
    'Product Types' => array('Non inventory item', 'Service'),
    'Weight' => 60,
  ),
  'class' => array(
    'Display' => 'Class',
    'Field' => 'ClassRef',
    'Type' => 'Class',
    'Contexts' => array('Sale' => 0),
    'Export Config' => 'uc_qb_add_class_method',
    'Product Types' => array('Non inventory item', 'Service'),
    'Group' => 'class',
    'Weight' => 70,
  ),
  'deposit_to' => array(
    'Display' => 'Bank Account',
    'Field' => 'DepositToAccountRef',
    'Type' => 'Account',
    'Account Type' => 'Bank',
    'Contexts' => array('Sale' => 0),
    'Group' => 'account',
    'Weight' => 80,
  ),
  'customer' => array(
    'Display' => 'Customer',
    'Field' => 'CustomerRef',
    'Type' => 'Customer',
    'Contexts' => array('Customer' => 0, 'Sale' => 0),
    'Group' => 'customer',
    'Weight' => 200,
  )
);

